﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Rando.Models
{
    public class Randomize : IRandomize
    {
        /// <summary>
        /// Returns a random number
        /// </summary>
        /// <returns></returns>
        int IRandomize.Random()
        {
            Random r = new Random();

            return r.Next();
        }
    }
}