﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Rando.Models
{
    public class RandoModel
    {
        [Key]
        public int numID { get; set; }

        
        public int MyNumber
        {
            get;
            set;
        }

        private string timeCreated;
        public string TimeCreated 
        { 
            get 
            { 
                return timeCreated ?? DateTime.Now.ToString(); 
            }
            set
            {
                timeCreated = value;
            }
        }
    }

    public class Randos
    {

        public RandoModel rando { get; set; }

        private List<RandoModel> numbers;
        public List<RandoModel> Numbers
        {
            get
            {
                if (numbers == null)
                {
                    return numbers = new List<RandoModel>();
                }
                else
                {

                    return numbers;
                }
            }
            set
            {
                numbers = value;
            }
        }
    }
}