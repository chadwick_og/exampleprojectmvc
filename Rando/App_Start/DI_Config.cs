﻿using Rando.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Unity;
using Unity.Mvc5;



namespace Rando.App_Start
{
    public static class DI_Config
    {
        public static void RegisterInjection()
        {
            var con = new UnityContainer();
            con.RegisterType<IRandomize, Randomize>();
            DependencyResolver.SetResolver(new UnityDependencyResolver(con));
        }
    }
}