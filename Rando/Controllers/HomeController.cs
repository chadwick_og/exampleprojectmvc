﻿using Rando.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Rando.Controllers
{
    public class HomeController : Controller
    {
        //gotta have that dependency injection
        private readonly IRandomize _rand;

        private RandoContext db = new RandoContext();

        public HomeController(IRandomize rand)
        {
            _rand = rand;
        }

        /// <summary>
        /// Home Page default loading
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {

            //populate the numbers we already have to display them
            /***
             * It's worth noting here I wouldn't typically do a database hit constantly like this
             * as it's very inefficient, it's just here to show use of entity framework
             * ***/
            Randos randos = new Randos()
            {
                Numbers = db.RandoModels.ToList(),
                rando = db.RandoModels.Count() > 0 ? db.RandoModels.FirstOrDefault() : new RandoModel()
            };

            return View(randos);
        }

        /// <summary>
        /// Gives us a random number to add to our list
        /// </summary>
        /// <param name="rando"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SubmitForm(RandoModel rando)
        {            
            //get the next random number (use that interface)
            rando.MyNumber = _rand.Random();

            //Add to the db
            db.RandoModels.Add(rando);

            //reload the page
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Saves a user entered number to our list
        /// </summary>
        /// <param name="rando"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SaveNum(RandoModel rando)
        {
            //add my number to the db
            db.RandoModels.Add(rando);

            //reload the page
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Modifies a given number
        /// </summary>
        /// <param name="rando"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ModifyNumber(RandoModel rando)
        {
            db.Entry(rando).State = EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Removes a number from our list
        /// </summary>
        /// <param name="numID"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult RemoveNumber(string numID)
        {
            RandoModel randomodel = db.RandoModels.Find(numID);
            db.RandoModels.Remove(randomodel);
            db.SaveChanges();

            return RedirectToAction("Index");
        }

    }
}
